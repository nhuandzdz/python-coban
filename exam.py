import requests
import html
import argparse
import os
import re

path = './exploit-db/'

def exploit_func(exploit_id):

    exploit_file = path+f"exploit_{exploit_id}.txt"
    if os.path.exists(exploit_file):
        try:
            os.system(f"xdg-open {exploit_file}")
        except Exception as e:
            pass
    else:
        exploit_url = f"https://www.exploit-db.com/exploits/{exploit_id}"
        headers = {
        'Accept': '*/*',
        'Accept-Language': 'en-US,en;q=0.9',
        'Connection': 'keep-alive',
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64)'

        }
        response = requests.get(exploit_url, headers=headers)
        if response.status_code == 200:

            exploit = response.text[response.text.find('<code') : response.text.find('</code>')]
            exploit = html.unescape(exploit[exploit.find('">') +2 :])

            with open(exploit_file, 'a') as file:
                file.write(str(exploit))
            try:
                 os.system(f"xdg-open {exploit_file}")
            except Exception as e:
                 pass

        elif response.status_code == 404:
           print(f'exploit-{exploit_id} does not exist')



def page_func(page):
    list_ex = []
    for f in os.listdir(path):
        list_ex.append(int(f[8:-4]))

    list_in_page = []
    for i in range(5):
     list_in_page.append(int(page)*5 + i +1)

    k = 0
    for i in sorted(list_ex):
        k += 1
        if k in list_in_page:
            print(i)

def func_search(string):

    for f in os.listdir(path):
        with open(path+f, 'r') as file:
            line = file.read()
        x = re.search(string.lower(), line.lower())
        if x:
            print(path +f)
def main():
    
    parser = argparse.ArgumentParser(description="Python Exam. ")
    parser.add_argument("--exploit", help="exploit ID")
    parser.add_argument("--page", help="get page")
    parser.add_argument("--search", help="Search keyword")
    
    args = parser.parse_args()
    if args.exploit :

        # ^ truoc, $ sau, \d+  tat ca ki tu co phai so k, |\ hoac sau \
        regex = r'^\d+|^https://www.exploit-db.com/exploits/\d+$'
        match = re.search(regex, args.exploit)  
        if match :
                
                regex2 = r'^\d+|\/(\d+)$'
                match = re.search(regex2, args.exploit)  

                start, end = match.span()
                if args.exploit[start] == '/':
                   exploit_func(args.exploit[start +1:end])
                else:
                   exploit_func(args.exploit[start:end])
        
        else:
            print('exploit syntax error!')

    elif args.page:

        regex = r'^\d+'
        match = re.search(regex, args.page)  

        if match :
                
                start, end = match.span()
                page_func(args.page[start:end])
        else:
            print('page syntax error!')

    elif args.search:
       
       func_search(args.search)

    else:
        parser.print_help()

if __name__ == "__main__":
    main()
